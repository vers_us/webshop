# Simple WEBSHOP

## Структура проекта

Данный проект основан на фреймворке Spring Boot.

Используемые технологии:
* Контейнер сервлетов - встроенный Tomcat.

* Доступ к данным:
  * БД - PostgreSQL
  * Фреймворк JPA - Hibernate 5.2.16

* Выполнение периодических задач - Quartz

## Настройка проекта

Основные настройки проекта находятся в файле __<APP_SHOP>\src\main\resources\application.properties__.

## Сборка

Перед сборкой проекта необходимо убедиться в наличии переменной окружения __JAVA_HOME__.

```shell
mvnw clean install
```

Так же сменив в pom-файле тип проекта на _war_ можно собрать проект для деплоя в контейнер сервлетов (Tomcat, Glassfish, Wildfly).

## Запуск

```shell
java -jar webshop-0.0.1-SNAPSHOT.jar
```

## Roadmap проекта

* Написать юнит-тесты
* Разделить проект на две части
  * Переписать GUI на Javascript (ReactJS, Angular, Vue)
  * Перейти на Api констроллеры или на Spring Data REST
* Добавить функционал пагинации

