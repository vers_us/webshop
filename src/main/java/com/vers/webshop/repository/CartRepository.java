/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vers.webshop.repository;

import com.vers.webshop.repository.dto.Cart;
import com.vers.webshop.repository.dto.Stats;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author vers_
 */
public interface CartRepository extends CrudRepository<Cart, Integer> {

    @Query(value = "select  date_trunc('hour', v.createdat) as at , count(v) as cartsCount, sum(v.price) as totalPrice, avg(v.price) as avgPrice, "
            + "sum(v.price-v.totalprice) as totalsumproc, "
            + "sum(v.totalprice) as totalpriceproc, avg(v.totalprice) as avgpriceproc "
            + "from cart v group by date_trunc('hour', v.createdat) order by date_trunc('hour', v.createdat) desc", nativeQuery = true)
    public List<Stats> aggregateStatistic();

}
