/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vers.webshop.repository;

import com.vers.webshop.repository.dto.CartItem;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author vers_
 */
public interface CartItemRepository extends CrudRepository<CartItem, Integer> {

    public List<CartItem> findAllByCartId(Integer cartId);
    @Transactional
    public void deleteByCartId(Integer cartId);
}
