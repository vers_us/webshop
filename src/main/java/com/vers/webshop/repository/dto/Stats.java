/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vers.webshop.repository.dto;

import java.util.Date;

/**
 *
 * @author vers_
 */
public interface Stats {

    public Date getAt();
    public Integer getCartsCount();
    public double getTotalPrice();
    public double getAvgPrice();
    public double getTotalSumProc();
    public double getTotalPriceProc();
    public double getAvgPriceProc();

}
