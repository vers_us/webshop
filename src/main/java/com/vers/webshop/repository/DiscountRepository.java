/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vers.webshop.repository;

import com.vers.webshop.repository.dto.Discount;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author vers_
 */
public interface DiscountRepository extends CrudRepository<Discount, Integer> {

    public List<Discount> findAllByOrderByStartDateDesc();

    public Discount findTop1ByOrderByStartDateDesc();

}
