/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vers.webshop.controllers;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.vers.webshop.repository.CartRepository;
import com.vers.webshop.repository.dto.Stats;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vers_
 */
@Controller
@RequestMapping("/statistic")
public class StatisticController {

    @Autowired
    CartRepository cartRepository;

    @RequestMapping()
    public ModelAndView index() {
        Map<String, Object> model = new HashMap<>();
        List<Stats> stats = cartRepository.aggregateStatistic();
        model.put("stats", stats);
        return new ModelAndView("statistic", model);
    }
}
