/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vers.webshop.controllers;

import com.vers.webshop.repository.DiscountRepository;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author vers_
 */
@Controller
@RequestMapping("/discounts")
public class DiscountController {

    @Autowired
    DiscountRepository discountRepository;

    @GetMapping(value = {"", "/"})
    public ModelAndView index() {
        Map<String, Object> model = new HashMap<>();
        model.put("name", "discounts");
        model.put("discounts", discountRepository.findAllByOrderByStartDateDesc());
        return new ModelAndView("discount", model);
    }
}
