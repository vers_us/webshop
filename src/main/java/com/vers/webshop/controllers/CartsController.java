/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vers.webshop.controllers;

import com.vers.webshop.repository.CartItemRepository;
import com.vers.webshop.repository.CartRepository;
import com.vers.webshop.repository.DiscountRepository;
import com.vers.webshop.repository.ProductRepository;
import com.vers.webshop.repository.dto.Cart;
import com.vers.webshop.repository.dto.CartItem;
import com.vers.webshop.repository.dto.Discount;
import com.vers.webshop.repository.dto.Product;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author vers_
 */
@Controller
@RequestMapping("/carts")
public class CartsController {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CartRepository cartRepository;

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    DiscountRepository discountRepository;

    @RequestMapping()
    public ModelAndView index() {
        Map<String, Object> model = new HashMap<>();
        model.put("carts", cartRepository.findAll());
        return new ModelAndView("carts", model);
    }

    @GetMapping("/new")
    public ModelAndView newCart() {
        Map<String, Object> model = new HashMap<>();
        model.put("cart", new Cart());
        return new ModelAndView("cartNew", model);
    }

    @PostMapping("/new")
    public ModelAndView newCartConfirm(Cart cart) {
        Map<String, Object> model = new HashMap<>();
        cart.setCreatedAt(new Date());
        cart = cartRepository.save(cart);
        return new ModelAndView("redirect:" + cart.getCartId(), model);
    }

    @GetMapping("/{id}")
    public ModelAndView getCart(@PathVariable("id") Integer cartId) {
        Map<String, Object> model = new HashMap<>();
        Cart cart = cartRepository.findById(cartId).get();
        cart.setCartItem(cartItemRepository.findAllByCartId(cartId));
        model.put("cartId", cartId);
        model.put("cart", cart);

        Discount discount = discountRepository.findTop1ByOrderByStartDateDesc();
        Iterable<Product> products = productRepository.findAll();
        if (discount != null) {
            products.forEach((t) -> {
                if (discount.getProductId().equals(t.getProductId())) {
                    t.setHasDiscount(true);
                    t.setDiscountValue(discount.getVal());
                }
            });
        }

        model.put("products", products);
        return new ModelAndView("cartEdit", model);
    }

    @PostMapping("/{id}/additem")
    public ModelAndView addItem(@PathVariable("id") Integer cartId, CartItem cartItem) {
        cartItemRepository.save(cartItem);
        return new ModelAndView("redirect:/carts/" + cartId);
    }

    @PostMapping("/{id}/confirm")
    public ModelAndView cartConfirm(@PathVariable("id") Integer cartId) {
        Cart cart = cartRepository.findById(cartId).get();
        cart.setPrice(0.0);
        cart.setTotalPrice(0.0);

        Discount currentDiscount = discountRepository.findTop1ByOrderByStartDateDesc();

        Map<Integer, CartItem> productItems = new HashMap<>();
        List<CartItem> items = cartItemRepository.findAllByCartId(cartId);
        items.forEach((ci) -> {
            if (!productItems.containsKey(ci.getProductId())) {
                productItems.put(ci.getProductId(), ci);
            }
        });
        Iterable<Product> products = productRepository.findAllById(productItems.keySet());

        products.forEach((prod) -> {
            double price = productItems.get(prod.getProductId()).getProductCount().doubleValue() * prod.getPrice();
            cart.setPrice(cart.getPrice() + price);
            System.out.printf("%f %f %f \n", productItems.get(prod.getProductId()).getProductCount().doubleValue(), prod.getPrice(), price);
            if (currentDiscount.getProductId().equals(prod.getProductId())) {
                cart.setTotalPrice(cart.getTotalPrice() + (price * currentDiscount.getVal().doubleValue() / 100.0));
                System.out.printf("%f %f \n",price, currentDiscount.getVal().doubleValue() / 100.0);
            } else {
                cart.setTotalPrice(price);
            }
        });
        System.out.printf(">> %f %f \n",cart.getPrice(), cart.getTotalPrice());
        cartRepository.save(cart);
        return new ModelAndView("redirect:/carts");
    }

    @PostMapping("/{id}/del")
    public ModelAndView deleteCart(@PathVariable("id") Integer cartId) {
        cartRepository.deleteById(cartId);
        cartItemRepository.deleteByCartId(cartId);
        return new ModelAndView("redirect:/carts");
    }

}
