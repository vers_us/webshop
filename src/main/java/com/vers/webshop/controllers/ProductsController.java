/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vers.webshop.controllers;

import com.vers.webshop.repository.DiscountRepository;
import com.vers.webshop.repository.ProductRepository;
import com.vers.webshop.repository.dto.Discount;
import com.vers.webshop.repository.dto.Product;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author vers_
 */
@Controller
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    DiscountRepository discountRepository;

    @RequestMapping()
    public ModelAndView index() {
        Map<String, Object> model = new HashMap<>();

        Discount discount = discountRepository.findTop1ByOrderByStartDateDesc();
        Iterable<Product> products = productRepository.findAll();
        if (discount != null) {
            products.forEach((t) -> {
                if (discount.getProductId().equals(t.getProductId())) {
                    t.setHasDiscount(true);
                    t.setDiscountValue(discount.getVal());
                }
            });
        }

        model.put("products", products);
        return new ModelAndView("products", model);
    }

    @GetMapping("/new")
    public ModelAndView newProduct() {
        Map<String, Object> model = new HashMap<>();
        model.put("name", "newProduct");
        model.put("product", new Product());
        return new ModelAndView("product", model);
    }

    @PostMapping("/new")
    public ModelAndView newProductConfirm(Product product) {
        productRepository.save(product);
        return new ModelAndView("redirect:/products");
    }

    @GetMapping("/{id}")
    public ModelAndView getProduct(@PathVariable("id") Integer productId) {
        Map<String, Object> model = new HashMap<>();
        model.put("productId", productId);
        model.put("edit", true);
        model.put("product", productRepository.findById(productId).get());
        return new ModelAndView("product", model);
    }

    @PostMapping("/{id}")
    public ModelAndView updateProduct(@PathVariable("id") Integer productId, Product product) {
        productRepository.save(product);
        return new ModelAndView("redirect:/products");
    }

    @PostMapping("/{id}/del")
    public ModelAndView deleteProduct(@PathVariable("id") Integer productId) {
        productRepository.deleteById(productId);
        return new ModelAndView("redirect:/products");
    }
}
