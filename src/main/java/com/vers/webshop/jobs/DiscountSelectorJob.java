/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vers.webshop.jobs;

import com.vers.webshop.repository.DiscountRepository;
import com.vers.webshop.repository.ProductRepository;
import com.vers.webshop.repository.dto.Discount;
import com.vers.webshop.repository.dto.Product;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author vers_
 */
@Component
public class DiscountSelectorJob implements Job {

    private static final Random RAND = new Random();
    @Autowired
    DiscountRepository discountRepository;

    @Autowired
    ProductRepository productRepository;

    @Value("${app.jobs.discount.min}")
    private int discountMin = 5;

    @Value("${app.jobs.discount.max}")
    private int discountMax = 10;

    public DiscountSelectorJob() {
    }

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        int discountVal = RAND.nextInt((discountMax - discountMin) + 1) + discountMin;

        List<Product> products = makeCollection(productRepository.findAll());
        if (products.isEmpty()) {
            return;
        }
        Integer productId = products.get(RAND.nextInt(products.size())).getProductId();

        Discount discount = new Discount();
        discount.setProductId(productId);
        discount.setStartDate(new Date());
        discount.setVal(discountVal);

        discountRepository.save(discount);
    }

    private static <E> List<E> makeCollection(Iterable<E> iter) {
        List<E> list = new ArrayList<>();
        for (E item : iter) {
            list.add(item);
        }
        return list;
    }
}
