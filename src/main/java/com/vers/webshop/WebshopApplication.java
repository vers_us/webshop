package com.vers.webshop;

import org.springframework.boot.Banner.Mode;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class WebshopApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(WebshopApplication.class).bannerMode(Mode.OFF).run(args);
    }
}
