/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  vers_
 * Created: May 6, 2018
 */

CREATE TABLE product (
    id SERIAL,
    title CHARACTER VARYING(64),
    description CHARACTER VARYING(255),
    price FLOAT,
    PRIMARY KEY (id)
);

CREATE TABLE discount (
    id SERIAL,
    startdate TIMESTAMP NOT NULL DEFAULT CURRENT_DATE,
    productid INT,
    val INT,
    PRIMARY KEY (id)
);

CREATE TABLE cart (
    id SERIAL,
    title CHARACTER VARYING(64),
    createdat TIMESTAMP NOT NULL DEFAULT CURRENT_DATE,
    totalprice FLOAT,
    price FLOAT,
    PRIMARY KEY (id)
);

CREATE TABLE cartitem (
    id SERIAL,
    cartid INT,
    productid INT,
    productcount INT,
    discountvalue FLOAT,
    PRIMARY KEY (id)
);